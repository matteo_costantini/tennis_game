package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;

public class GameTest {

	@Test
	public void fifteenThirty() throws Exception {
		// Arrange
		Game game = new Game("Federer","Nadal");
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		// Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		String status = game.getGameStatus();
		// Assert
		assertEquals("Federer fifteen - Nadal thirty",status);
	}
	
	@Test
	public void fortyThirty() throws Exception {
		// Arrange
		Game game = new Game("Federer","Nadal");
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		// Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		String status = game.getGameStatus();
		// Assert
		assertEquals("Federer forty - Nadal thirty",status);
	}
	
	@Test
	public void federerWins() throws Exception {
		// Arrange
		Game game = new Game("Federer","Nadal");
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		// Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		String status = game.getGameStatus();
		// Assert
		assertEquals("Federer wins",status);
	}
	
	@Test
	public void fifteenForty() throws Exception {
		// Arrange
		Game game = new Game("Federer","Nadal");
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		// Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		String status = game.getGameStatus();
		// Assert
		assertEquals("Federer fifteen - Nadal forty",status);
	}
	
	@Test
	public void deuceForty() throws Exception {
		// Arrange
		Game game = new Game("Federer","Nadal");
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		// Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		String status = game.getGameStatus();
		// Assert
		assertEquals("Deuce",status);
	}
	
	@Test
	public void federeradvantage() throws Exception {
		// Arrange
		Game game = new Game("Federer","Nadal");
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		// Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName1);
		String status = game.getGameStatus();
		// Assert
		assertEquals("Advantage Federer",status);
	}
	
	@Test
	public void nadalWins() throws Exception {
		// Arrange
		Game game = new Game("Federer","Nadal");
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		// Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		String status = game.getGameStatus();
		// Assert
		assertEquals("Nadal wins",status);
	}
	
	@Test
	public void nadalAdvantage() throws Exception {
		// Arrange
		Game game = new Game("Federer","Nadal");
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		// Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		String status = game.getGameStatus();
		// Assert
		assertEquals("Advantage Nadal",status);
	}
	
	@Test
	public void deuceFourPoints() throws Exception {
		// Arrange
		Game game = new Game("Federer","Nadal");
		String playerName1 = game.getPlayerName1(); 
		String playerName2 = game.getPlayerName2();
		// Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName2);
		String status = game.getGameStatus();
		// Assert
		assertEquals("Deuce",status);
	}
}